(function () {

	$(document).ready(function () {

		$('ul.sf-menu').superfish();

		var meanMenu = $('.o-hero__menu').meanmenu({
			meanScreenWidth: 992,
			meanMenuContainer: '#menu'
		});

		var slideout = new Slideout({
			'panel': document.getElementById('panel'),
			'menu': document.getElementById('menu'),
			'padding': 256,
			'tolerance': 70
		});

		// Toggle button
		$('.toggle-button').on('click', function() {
			slideout.toggle();
			$('#i-hamburger').toggleClass('active');
		});
		slideout.on('close', function() {
			$('#i-hamburger').removeClass('active');
		});
		slideout.on('open', function() {
			$('#i-hamburger').addClass('active');
		});

	});

}());